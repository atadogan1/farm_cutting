﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAveragePositions : MonoBehaviour {

    public Transform[] players;

    Vector3 averagePosition;
    CameraScript cameraScript;


    public float defaultLerpRate = 0.9f;

    private void Awake()
    {
        cameraScript = Camera.main.GetComponentInChildren<CameraScript>();
    }

    // Use this for initialization
    void Start () {
        /*
        if (PlayerManager.Instance.numberofPlayers == 1)
        {
            players = new Transform[] { PlayerManager.Instance.player1.transform };
        }
        else
        {
            players = new Transform[] { PlayerManager.Instance.player1.transform, PlayerManager.Instance.player2.transform };
        }*/
        /*
        foreach (Transform player in players)
        {
            if (player == null)
            {
                Debug.Log("cannot have null players");
            }
        }*/
       // CalculateAveragePosition();
        MoveToAveragePosition();
	}

    void CalculateAveragePosition()
    {
        Vector3 positionSum = Vector3.zero;

        for (int i = 0; i < players.Length; i++)
        {
            positionSum += players[i].position;
        }
        averagePosition = positionSum/ players.Length;
          //  = (players[0].position + players[1].position+ players[2].position + players[3].position) / 4;
        


        /*
        foreach(Transform player in players)
        {
            if(player != null)
            positionSum += player.position;
        }

        averagePosition = positionSum / players.Length;*/
    }

    void MoveToAveragePosition(float lerpRate = 0.1f)
    {
        CalculateAveragePosition();
        transform.position = Vector3.Lerp(transform.position, averagePosition, lerpRate);
    }

    float GiveCameraDistance()
    {
        //only works with 2 player as of yet
       
        float playerDistance = Vector3.Distance(players[0].position, players[1].position);
        return playerDistance;
    }

    private void Update()
    {
       /* if (cameraScript != null)
        {
            cameraScript.GetPlayerDistance(GiveCameraDistance());
        }*/

    }

    // Update is called once per frame
    void FixedUpdate () {
        MoveToAveragePosition();

    }
}
