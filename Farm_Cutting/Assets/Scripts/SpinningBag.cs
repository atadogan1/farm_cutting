﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinningBag : MonoBehaviour {

    public GameObject thrownSeedsPrefab;
   

    Rigidbody rbBag;

    [SerializeField]
    private float speedToThrow;
    [SerializeField]
    private float throwingSpeed=1;
    [SerializeField]
    private float cooldown = 1;

    float timer;

    public int currentSeedNumber;
    public int maxSeedsNumber = 100;

    public int totalSeedPlanted = 0;
    
    void OnEnable()
    {
        rbBag.centerOfMass = new Vector3(0.0f, -0.32f, -1.06f);
    }

    private void Awake()
    {
        rbBag = GetComponent<Rigidbody>();
        rbBag.centerOfMass = new Vector3(0.0f, -0.32f, -1.06f);
        currentSeedNumber = maxSeedsNumber;
    }

    // Update is called once per frame
    void Update () {
        timer += Time.deltaTime;

        Vector3 bagSpeed = rbBag.velocity;
        if (bagSpeed.magnitude>speedToThrow && timer>cooldown && currentSeedNumber > 0)
        {
            GameObject seed = Instantiate(thrownSeedsPrefab, rbBag.position, Quaternion.identity);

            seed.GetComponent<SeedController>().playerNumber = (int)GetComponentInParent<PlayerController>().playerNumber;
            seed.GetComponent<Rigidbody>().velocity = rbBag.velocity * throwingSpeed;
            timer = 0;
        }
	}


    private void FixedUpdate()
    {
        
    }
}
