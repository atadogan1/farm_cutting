﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{

    public static PlayerManager instance;


    public List<GameObject> listOfPlayers = new List<GameObject>();


    private void Awake()
    {
        //establishing singleton --peter lemme know if this is right
        if (instance == null)
        {
            instance = this;
        }
        //you need to say else if instance != this, then destory self --Peter
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SpawnPlayers()
    {

    }

}
