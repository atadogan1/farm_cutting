﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;


[RequireComponent(typeof(PlayerMovement))]
public class PlayerController : MonoBehaviour
{


    public XboxController playerNumber = XboxController.First;

    PlayerMovement playerMovement;

    public GameObject bag;
    public GameObject scythe;


    void Awake()
    {
        playerMovement = GetComponent<PlayerMovement>();


    }

    void Update()
    {
        SetMovement();
    }

    private void FixedUpdate()
    {
        SetRotation();
    }


    public void ChangeToCut()
    {
        bag.SetActive(false);
        scythe.SetActive(true);
    }

    public void ChangeToPlant()
    {
        bag.SetActive(true);
        scythe.SetActive(false);
    }

    public void ChangeToNeutral()
    {
        bag.SetActive(false);
        scythe.SetActive(false);
    }

    void SetMovement()
    {


        Vector3 _movement = new Vector3(XCI.GetAxis(XboxAxis.LeftStickX, playerNumber), 0, XCI.GetAxis(XboxAxis.LeftStickY, playerNumber));
        
        //_movement += new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        //Quaternion cameraRotate = Quaternion.Euler(new Vector3(0, Camera.main.transform.rotation.y, 0));
        //_movement = cameraRotate* _movement;
        //Debug.Log(_movement);

        _movement = Camera.main.transform.TransformDirection(_movement);
        _movement.y = 0;
        playerMovement.SetMovementInput(_movement);
    }

    void SetRotation()
    {
        
        Vector3 _rotation = new Vector3(XCI.GetAxis(XboxAxis.RightStickX, playerNumber), 0, XCI.GetAxis(XboxAxis.RightStickY, playerNumber));

        //_rotation += new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        _rotation = Camera.main.transform.TransformDirection(_rotation);
        _rotation.y = 0;
        playerMovement.SetRotationInput(_rotation);

    }


}

