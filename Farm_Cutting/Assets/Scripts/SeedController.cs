﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeedController : MonoBehaviour {

    public int playerNumber;

    List<SpinningBag> spinningBag = new List<SpinningBag>();


    void Start()
    {
        spinningBag.Clear();

        int i = 0;
        foreach (GameObject item in PlayerManager.instance.listOfPlayers)
        {
            spinningBag.Add(PlayerManager.instance.listOfPlayers[i].GetComponentInChildren<SpinningBag>());
            i++;
        }
    }

    // Update is called once per frame
    void Update ()
    {
		
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Ground")
        {
            if (GridManager.instance.currentGrass[GridManager.instance.GridDetection(transform.position)] < GridManager.instance.maxGrassNumOnSingleGrid)
            {
                switch (playerNumber)
                {
                    case 1:
                        PlantSeeds.PlantSeed(GridManager.instance.GridDetection(transform.position), "Player1", 1);

                        spinningBag[0].currentSeedNumber--;
                        break;
                    case 2:
                        PlantSeeds.PlantSeed(GridManager.instance.GridDetection(transform.position), "Player2", 2);
                        spinningBag[1].currentSeedNumber--;
                        break;
                    case 3:
                        PlantSeeds.PlantSeed(GridManager.instance.GridDetection(transform.position), "Player3", 3);
                        spinningBag[2].currentSeedNumber--;
                        break;
                    case 4:
                        PlantSeeds.PlantSeed(GridManager.instance.GridDetection(transform.position), "Player4", 4);
                        spinningBag[3].currentSeedNumber--;
                        break;
                    default:
                        break;
                }
                
                GridManager.instance.currentGrass[GridManager.instance.GridDetection(transform.position)]++;
            }
            Destroy(gameObject);
        }
    }
}
