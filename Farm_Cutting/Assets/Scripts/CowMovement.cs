﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CowMovement : MonoBehaviour {

    public enum CowFeels { Eating, Chasing, Charging };

    public CowFeels cowFeel;

    private Rigidbody rbCow;

    private void Awake()
    {
        rbCow = GetComponent<Rigidbody>();
    }


    private void FixedUpdate()
    {
        switch (cowFeel)
        {
            case CowFeels.Eating:

                break;
            case CowFeels.Chasing:

                break;
            case CowFeels.Charging:

                break;
            default:
                break;
        }
    }
}
