﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FightGameMode : MonoBehaviour {

    [SerializeField]
    private GameObject gameModeAllUI;

    [SerializeField]
    private GameObject gameRoundStartUI;
    [SerializeField]
    private GameObject gameRoundEndUI;

    [SerializeField]
    float titleAnimTime = 1;
    [SerializeField]
    float titleStayTime = 1;

    [HideInInspector]
    public float roundTime;
    [SerializeField]
    public float maxRoundTime = 20;

    public static System.Action fightGameModeEnd;

    // Use this for initialization
    void Awake()
    {
        ResetUI();
    }

    void OnEnable()
    {
        //Start Plant Game mode shit
        //RoundOver();
        roundTime = maxRoundTime;

       

        StartCoroutine(StartRound());

    }

    void ResetUI()
    {
        gameModeAllUI.SetActive(false);
        //gameRoundStartUI.transform.localScale = Vector3.zero;
        //gameRoundEndUI.transform.localScale = Vector3.zero;
    }


    IEnumerator StartRound()
    {
        
        gameModeAllUI.SetActive(true);
        //  gameRoundStartUI.transform.DOScale(Vector3.one, titleAnimTime);
        //  yield return new WaitForSeconds(titleStayTime);
        //  gameRoundStartUI.transform.DOScale(Vector3.zero, titleAnimTime);
          yield return new WaitForSeconds(titleAnimTime);
        GameManager.instance.ChangeAllPlayersToCut();
        //  gameModeAllUI.transform.DOScale(Vector3.one, titleAnimTime);


        while (true)
        {
            if (roundTime <= 0)
            {
                break;
            }

            //update game logic here
            GameModeLogic();

            //update UI here
            UpdateGameUI();


            roundTime -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        //Set Player to have nothing


        //  gameModeAllUI.transform.DOScale(Vector3.zero, titleAnimTime);
        //  yield return new WaitForSeconds(titleAnimTime);

        //Game Round End 
        //  gameRoundEndUI.transform.DOScale(Vector3.one, titleAnimTime);
        //  yield return new WaitForSeconds(titleStayTime);
        //  gameRoundEndUI.transform.DOScale(Vector3.zero, titleAnimTime);

        //gameModeAllUI.SetActive(false);
        fightGameModeEnd();
        GameManager.instance.ChangeAllPlayersToNeutral();
        yield return new WaitForSeconds(3);
        RoundOver();
    }


    //put game mode logic here
    void GameModeLogic()
    {


    }

    //put game mode UI's here
    void UpdateGameUI()
    {


    }

    void TurnOffSeedUI()
    {


    }

    void RoundOver()
    {
        
        this.enabled = false;

    }
    void OnDisable()
    {
        //Disable Plant Game mode shit
        gameModeAllUI.SetActive(false);
        StopAllCoroutines();
        GameManager.instance.GoToNextRound();

    }

    // Update is called once per frame
    void Update()
    {

    }
}
