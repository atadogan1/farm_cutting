﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CropCount : MonoBehaviour {


    public int GetCropCount()
    {
        return PoolManager.instance.pools["Player" + (int)GetComponentInParent<PlayerController>().playerNumber].CurrentAliveGrassNumber();
    }

}
