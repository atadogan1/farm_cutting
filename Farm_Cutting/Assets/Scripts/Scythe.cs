﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scythe : MonoBehaviour {

    // public GameObject scytheObject

    void OnEnable()
    {
        GetComponent<Rigidbody>().centerOfMass = new Vector3(0.06f, -0.28f, -0.25f);
        // scytheObject.SetActive(true);
    }
    void OnDisable()
    {
      //  scytheObject.SetActive(false);
    }

    void Awake()
    {
        GetComponent<Rigidbody>().centerOfMass = new Vector3(0.06f, -0.28f, -0.25f);
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            StartCoroutine(PlayerFlys(col));
        }
    }

    IEnumerator PlayerFlys(Collision col)
    {
        //col.gameObject.GetComponentInChildren<PlayerMovement>().enabled = false;
        //col.gameObject.GetComponentInChildren<Rigidbody>().AddForce(Vector3.up * 10, ForceMode.Impulse);
        yield return null;
        //yield return new WaitForEndOfFrame;
        //col.gameObject.GetComponentInChildren<PlayerController>().enabled = true;
    }

    //do i enable collider or object

    /* private void OnTriggerEnter(Collider other)
      {
          if (other.gameObject.CompareTag("Grass"))
          {
              Debug.Log("grass cut");
             other.gameObject.SetActive(false);
          }

           if (other.GetComponentInParent<PlayerController>().playerNumber!= GetComponentInParent<PlayerController>().playerNumber)
           {
               Debug.Log("player cutt "+ other.GetComponentInParent<PlayerController>().playerNumber);
               other.GetComponentInParent<PlayerMovement>().ScytheImpact(other.attachedRigidbody);
             // Debug.Log()
           }


                 if (other.gameObject.CompareTag("Player"))
                 {
                     Debug.Log("playerbump");
                     other.GetComponentInParent<PlayerMovement>().ScytheImpact(other.attachedRigidbody);
                 }

                 if (other.gameObject.CompareTag("Scythe"))
                 {
                     Debug.Log("scythe impact");
                     other.GetComponentInParent<PlayerMovement>().ScytheImpact(other.attachedRigidbody);
                 }
             }*/

    /*
        void OnCollisionEnter(Collision collision)
        {
          //  Debug.Log("collision?");
            //Debug.Log(collision.contacts[0].thisCollider.tag);
            if (collision.contacts[0].thisCollider.CompareTag("Scythe"))
            {

                if (collision.collider.gameObject.CompareTag("Grass"))

                {
                    // Debug.Log("grass cut");

                    collision.gameObject.SetActive(false);
                }


                //if the thing we hit is a player
                if (collision.collider.gameObject.CompareTag("Player"))

                {

                   // Debug.Log("playa");
                    //do push when we hit them with a scythe

                    collision.gameObject.GetComponentInParent<PlayerMovement>().GotHitByAMansScythe(GetComponent<Rigidbody>());
                }


                //if the thing we hit is a scythe
                if (collision.collider.gameObject.CompareTag("Scythe"))

                {

                    //    repel when we hit a scythe with other scythe

                    // Debug.Log("this is a scythe of  " + collision.gameObject.GetComponentInParent<PlayerController>().playerNumber + " hitting " +this.gameObject.tag);

                    collision.gameObject.GetComponentInParent<PlayerMovement>().RepelScythe(GetComponent<Rigidbody>());
                }
            }

        }
        */


    float timer;

    public int cutCount;
    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.GetComponentInParent<Grass>())

        {
            // Debug.Log("grass cut");

            if (GetComponentInParent<PlayerMovement>().spin)
            {

                Vector3 cross = Vector3.Cross( Vector3.up, (other.transform.position - GetComponent<Rigidbody>().position));

                //+ Vector3.up * 2.5f

                if(!other.gameObject.GetComponentInParent<Grass>().alreadyCut)
                {
                    other.gameObject.GetComponentInParent<Grass>().GotCut(cross * Mathf.Clamp((GetComponentInParent<PlayerMovement>().angleDiffAvg), -20, 20) * 0.01f, (int)GetComponentInParent<PlayerController>().playerNumber);
                    cutCount++;
                }
            }
        }
    }
}
