﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class GetReadyUI : MonoBehaviour
{


    public GameObject get;
    public GameObject ready;
    public GameObject prompt;

    public string phasePrompt;

    // Use this for initialization
    void OnEnable()
    {
        StopCoroutine("PopUp");
        StartCoroutine(PopUp());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator PopUp()
    {
        //Debug.Log("run");
        // prompt.
        Reset();

        yield return new WaitForSeconds(0.3f);
        get.transform.DOScale(1, 0.6f).SetEase(Ease.OutBack);
        get.transform.DOPunchRotation(new Vector3(0, 0, -5f), 0.6f, 2);
        //yield return new WaitForSeconds(1f);

        TextMeshProUGUI TMget = get.GetComponent<TextMeshProUGUI>();
        TMget.DOColor(new Color(TMget.color.r, TMget.color.g, TMget.color.b, 0), 2).SetDelay(1.8f);

        ready.transform.DOScale(1, 0.6f).SetEase(Ease.OutBack).SetDelay(0.8f);
        ready.transform.DOPunchRotation(new Vector3(0, 0, -5f), 0.6f, 2).SetDelay(0.8f);

        TextMeshProUGUI TMready = ready.GetComponent<TextMeshProUGUI>();
        TMready.DOColor(new Color(TMready.color.r, TMready.color.g, TMready.color.b, 0), 2).SetDelay(1.8f);
        //yield return new WaitForSeconds(1.5f);

        prompt.transform.DOScale(1, 1.6f).SetEase(Ease.OutBounce).SetDelay(2.5f);
        //prompt.transform.DOPunchRotation(new Vector3(0, 0, 2f), 1.3f, 2).SetDelay(1.2f);

        TextMeshProUGUI TMprompt = prompt.GetComponent<TextMeshProUGUI>();
        TMprompt.DOColor(new Color(TMprompt.color.r, TMprompt.color.g, TMprompt.color.b, 0), 2).SetDelay(6f);

        /*
        float lerp = 1;
        while (lerp > 0)
        {
            //Debug.Log("working");
            TextMeshProUGUI TMget = get.GetComponent<TextMeshProUGUI>();
            TMget.color = new Color(TMget.color.r, TMget.color.g, TMget.color.b, lerp);

            TextMeshProUGUI TMready = ready.GetComponent<TextMeshProUGUI>();
            TMready.color = new Color(TMready.color.r, TMready.color.g, TMready.color.b, lerp);

            TextMeshProUGUI TMprompt = prompt.GetComponent<TextMeshProUGUI>();
            TMprompt.color = new Color(TMprompt.color.r, TMprompt.color.g, TMprompt.color.b, lerp);

            yield return null;
            lerp -= Time.deltaTime / 3;
        }
        */

    }

    public void Reset()
    {
        get.transform.localScale = Vector3.zero;
        ready.transform.localScale = Vector3.zero;
        prompt.transform.localScale = Vector3.zero;

        TextMeshProUGUI TMget = get.GetComponent<TextMeshProUGUI>();
        TMget.color = new Color(TMget.color.r, TMget.color.g, TMget.color.b, 1);

        TextMeshProUGUI TMready = ready.GetComponent<TextMeshProUGUI>();
        TMready.color = new Color(TMready.color.r, TMready.color.g, TMready.color.b, 1);

        TextMeshProUGUI TMprompt = prompt.GetComponent<TextMeshProUGUI>();
        TMprompt.color = new Color(TMprompt.color.r, TMprompt.color.g, TMprompt.color.b, 1);
    }
}
