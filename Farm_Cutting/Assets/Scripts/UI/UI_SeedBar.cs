﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;

public class UI_SeedBar : MonoBehaviour
{

    Vector3 endPosition;
    Vector3 startPosition;

    SpinningBag bag;

    public float fillPercent;
    bool flashing;
    bool outOfSeeds;

    public TextMeshPro text;
    Vector3 originalTextScale;
    Slider slider;

    Vector3 originalBarScale;


    void Awake()
    {
        originalTextScale = text.transform.parent.localScale;
        slider = GetComponent<Slider>();
        originalBarScale = transform.parent.localScale;
        bag = transform.parent.parent.GetComponentInChildren<SpinningBag>();
    }
    // Use this for initialization
    void Start()
    {
        fillPercent = 1;
        // startPosition = transform.localPosition;
        //  endPosition = new Vector3(-2.7f, 0, 0);

        //  transform.parent.localScale = Vector3.zero;
    }

    void OnEnable()
    {

        PlantGameMode.plantGameModeStart += ShowBar;
        PlantGameMode.plantGameModeEnd += DisableBar;
    }


    void OnDisable()
    {
        PlantGameMode.plantGameModeStart -= ShowBar;
        PlantGameMode.plantGameModeEnd -= DisableBar;
    }

    public void ShowBar()
    {
        outOfSeeds = false;
        text.text = "seeds";
        transform.parent.DOScale(originalBarScale, 0.4f).SetEase(Ease.OutCubic);
    }

    public void DisableBar()
    {
        transform.parent.DOScale(0, 0.4f).SetEase(Ease.OutCubic);
    }

    // Update is called once per frame
    void Update()
    {
        fillPercent =  (float)bag.currentSeedNumber/ (float)bag.maxSeedsNumber;

        //fillPercent -= Time.deltaTime / 5;

        UpdateBar(fillPercent);
    }

    public void UpdateBar(float fillPercent)
    {

        /*
        if (!outOfSeeds)
            {
                transform.localPosition = Vector3.Lerp(endPosition, startPosition, fillPercent);
                if (fillPercent <= 0)
                {
                    transform.position = new Vector3(-3.2f, 0, 0);
                    ShrinkText();
                }

                if (fillPercent <= 0.4f && !flashing)
                {
                    //WHEN THE SEEDS ARE LOW, FLASH COLOUR
                    BeginFlashing();
                }
            }
            */

        if (!outOfSeeds)
        {
            slider.value = fillPercent;
            if (fillPercent <= 0)
            {
                ShrinkText();
            }
        }



    }


    void ShrinkText()
    {
        outOfSeeds = true;
         
        text.transform.parent.DOScale(0, 0.4f).OnComplete(EnlargeText).SetEase(Ease.OutCubic);

    }

    void EnlargeText()
    {
        // Debug.Log(originalTextScale.x);
        text.text = "no seeds!";
        // DOTween.KillAll();
        text.transform.parent.DOScale(originalTextScale, 0.4f).SetEase(Ease.OutCubic);
    }


    void BeginFlashing()
    {
        flashing = true;
        gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 0.467555f, 0.3632075f, 1), 0.2f).SetLoops(-1, LoopType.Yoyo);
    }
}
