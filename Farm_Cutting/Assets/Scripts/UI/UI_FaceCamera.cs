﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_FaceCamera : MonoBehaviour {


    Camera camera;

    public bool vertical;

    private void Awake()
    {
        camera = Camera.main;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
        FaceCamera();

    }

    void FaceCamera()
    {
        transform.rotation = camera.transform.rotation;

        
    }
}
