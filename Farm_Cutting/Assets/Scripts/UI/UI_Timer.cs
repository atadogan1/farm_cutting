﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class UI_Timer : MonoBehaviour {


    float timer;
    float maxTime;
    Image circleIcon;

    Vector3 originalRect;

    bool isRunning;

    void Awake()
    {
        circleIcon = GetComponent<Image>();
        originalRect = circleIcon.rectTransform.localScale;
    }

	// Use this for initialization
	void Start () {
        

    }
	
	// Update is called once per frame
	void Update () {
        UpdateTimer();
	}

    void OnEnable()
    {
        isRunning = true;

        circleIcon.rectTransform.localScale = Vector3.zero;
        circleIcon.rectTransform.DOScale(originalRect, 2).SetEase(Ease.OutBack).SetDelay(4f);
        //fade in the timer text, first childed object
        circleIcon.GetComponentInChildren<TextMeshProUGUI>().alpha = 0;
        circleIcon.GetComponentInChildren<TextMeshProUGUI>().DOFade(1, 1).SetDelay(8);
    }

    void UpdateTimer()
    {
        if (GetComponentInParent<PlantGameMode>())
        {
            timer = GetComponentInParent<PlantGameMode>().roundTime;
            maxTime = GetComponentInParent<PlantGameMode>().maxRoundTime;
        }
        else if (GetComponentInParent<FightGameMode>())
        {
            timer = GetComponentInParent<FightGameMode>().roundTime;
            maxTime = GetComponentInParent<FightGameMode>().maxRoundTime;
        }

        circleIcon.fillAmount = timer / maxTime;

        if (timer <= 0 && isRunning)
        {
            TweenOut();
        }

    }

   void TweenOut()
    {
        isRunning = false;
        circleIcon.rectTransform.DOScale(0, 0.5f).SetEase(Ease.OutCubic);
    }
}
