﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UI_BobRepeat : MonoBehaviour
{

    public float bobAmount = 1;
    public float loopTime = 4;

    //public float startPosition;

    // Use this for initialization
    void Start()
    {
        //startPosition = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnEnable()
    {
        BobUp();
    }

    void BobUp()
    {
        gameObject.transform.DOMoveY(transform.position.y + bobAmount, loopTime).OnComplete(BobDown).SetEase(Ease.InOutSine);
    }

    void BobDown()
    {
        gameObject.transform.DOMoveY(transform.position.y - bobAmount, loopTime).OnComplete(BobUp).SetEase(Ease.InOutSine);
    }
}
