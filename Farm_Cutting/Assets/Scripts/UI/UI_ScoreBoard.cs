﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class UI_ScoreBoard : MonoBehaviour {


    Scythe scythe;
    TextMeshPro scoreText;
    float score;
    void OnEnable()
    {
        PlantGameMode.plantGameModeEnd += ShowScore;
        FightGameMode.fightGameModeEnd += ShowScore;

    }

    void OnDisable()
    {
        PlantGameMode.plantGameModeEnd -= ShowScore;
        FightGameMode.fightGameModeEnd -= ShowScore;

    }
    void Awake()
    {
        //scythe = transform.parent.GetComponentInChildren<Scythe>();
        scoreText = GetComponentInChildren<TextMeshPro>();
    }

	// Use this for initialization
	void Start () {
       // ShowScore();
        transform.localScale = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowScore()
    {
        transform.DOScale(1, 0.7f).SetEase(Ease.InOutQuad).OnComplete(HideScore);
        UpdateScore();
    }

    public void HideScore()
    {
        transform.DOScale(0, 0.7f).SetEase(Ease.InOutQuad).SetDelay(4f);
    }


    void UpdateScore()
    {
        score = GetComponentInParent<CropCount>().GetCropCount();
        scoreText.text = score.ToString();
    }
}
