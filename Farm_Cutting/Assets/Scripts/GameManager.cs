﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private void Awake()
    {
        //establishing singleton --peter lemme know if this is right
        if (instance == null)
        {
            instance = this;
        }
        //you need to say else if instance != this, then destory self --Peter
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this);
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.R))
        {


            ResetGame();

        }
    }

    int playerCount = 4;


    //Called after ready up
    [Button("Start")]
    public void StartGame()
    {
        // Debug.Log("Game Start");
        //Load Game Scene
        //CHANGE buildIndex BACK TO +1
        StartCoroutine(LoadGameplayLevel());
        //After Load

    }

    AsyncOperation asyncLoadLevel;

    IEnumerator LoadGameplayLevel()
    {
        yield return new WaitForSeconds(1.5f);

        asyncLoadLevel = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
        while (!asyncLoadLevel.isDone)
        {
           // print("Loading the Scene");
            yield return null;
        }

        FindAllSpinningBags();
        ChangeAllPlayersToNeutral();

        GoToNextRound();

    }

    [SerializeField]
    public List<MonoBehaviour> ScenesToGoThrough = new List<MonoBehaviour>();


    //public PlantGameMode plantGameMode;

    //public FightGameMode fightGameMode;


    public void FinishedGameMode()
    {
        GoToNextRound();
    }


    int currentRoundCount;

    public void GoToNextRound()
    {

        if (currentRoundCount < ScenesToGoThrough.Count)
        {
            ScenesToGoThrough[currentRoundCount].enabled = true;
        }
        else
        {
            StartCoroutine(GameOver());
        }
        currentRoundCount++;
    }



    [SerializeField]
    GameObject endGameUI;
    [SerializeField]
    float scoreboardAnimTime = 1;
    [SerializeField]
    float scoreboardStayTime = 1;

    void ResetEngGameUI()
    {
        endGameUI.SetActive(false);
        endGameUI.transform.localScale = Vector3.zero;
    }

    IEnumerator GameOver()
    {
        //Show end screen stats
        //endGameUI.transform.DOScale(Vector3.one, scoreboardAnimTime);
        //yield return new WaitForSeconds(scoreboardStayTime);
        //endGameUI.transform.DOScale(Vector3.zero, scoreboardAnimTime);
        //yield return new WaitForSeconds(scoreboardStayTime);

        //disable All Player controls
        //This function is currently Empty
        DisableAllPlayersControls();

        //SHOW UI

        GameEndUIandInfo();

        yield return new WaitForSeconds(15);

        StartCoroutine(PopOutEndGameUI());

        //resets game
        ResetGame();
    }

    void DisableAllPlayersControls()
    {
        if (PlayerManager.instance)
        {
            foreach (GameObject item in PlayerManager.instance.listOfPlayers)
            {
                item.GetComponentInChildren<PlayerController>().enabled = false;
                item.GetComponentInChildren<PlayerMovement>().enabled = false;
            }
        }
        else
        {
            Debug.LogError("PlayerManager not found");
        }
    }

    void EnableAllPlayersControls()
    {
        if (PlayerManager.instance)
        {
            foreach (GameObject item in PlayerManager.instance.listOfPlayers)
            {
                item.GetComponentInChildren<PlayerController>().enabled = true;
                item.GetComponentInChildren<Movement>().enabled = true;
            }
        }
        else
        {
            Debug.LogError("PlayerManager not found");
        }
    }

    public void ChangeAllPlayerToPlant()
    {
        if (PlayerManager.instance)
        {
            foreach (GameObject item in PlayerManager.instance.listOfPlayers)
            {
                item.GetComponentInChildren<PlayerController>().ChangeToPlant();
            }
        }
        else
        {
            Debug.LogError("PlayerManager not found");
        }
    }

    public void ChangeAllPlayersToCut()
    {
        if (PlayerManager.instance)
        {
            foreach (GameObject item in PlayerManager.instance.listOfPlayers)
            {
                item.GetComponentInChildren<PlayerController>().ChangeToCut();
            }
        }
        else
        {
            Debug.LogError("PlayerManager not found");
        }
    }

    public void ChangeAllPlayersToNeutral()
    {
        if (PlayerManager.instance)
        {
            foreach (GameObject item in PlayerManager.instance.listOfPlayers)
            {
                item.GetComponentInChildren<PlayerController>().ChangeToNeutral();
            }
        }
        else
        {
            Debug.LogError("PlayerManager not found");
        }
    }



    public List<SpinningBag> spinningbags = new List<SpinningBag>();

    public void FindAllSpinningBags()
    {
        if (PlayerManager.instance)
        {
            spinningbags.Clear();

            foreach (GameObject item in PlayerManager.instance.listOfPlayers)
            {
               // print(item.GetComponentInChildren<SpinningBag>());
                spinningbags.Add(item.GetComponentInChildren<SpinningBag>());
            }
        }
        else
        {
            Debug.LogError("PlayerManager not found");
        }
    }

    public void FillUpAllSeeds()
    {
        if (PlayerManager.instance)
        {
            foreach (SpinningBag item in spinningbags)
            {
                item.gameObject.SetActive(true);
                item.currentSeedNumber = item.maxSeedsNumber;
                item.gameObject.SetActive(false);
            }
        }
        else
        {
            Debug.LogError("PlayerManager not found");
        }

    }

    void ResetGame()
    {


        for (int i = 0; i < ScenesToGoThrough.Count; i++)
        {
            ScenesToGoThrough[i].enabled = false;
        }

        foreach (GetReadyUI UI in GetComponentsInChildren<GetReadyUI>())
        {
            UI.Reset();
        }

        StartCoroutine(PopOutEndGameUI());

        StopAllCoroutines();

        currentRoundCount = 0;

        SceneManager.LoadScene(0);




    }

    void GameEndUIandInfo()
    {
        GetAllPlayerInfo();
        PopulateUI();
        SetPositionEndGameUI();
        PopUpGameEndUI();
    }

    [SerializeField]
    List<Text> positionUIText = new List<Text>();
    [SerializeField]
    List<Text> cropsLeftUIText = new List<Text>();
    [SerializeField]
    List<Text> cropsCutUIText = new List<Text>();
    [SerializeField]
    List<Text> cropsPlantedUIText = new List<Text>();


    void PopulateUI()
    {
        for (int i = 0; i < playerCount; i++)
        {
            //Crops Left UI
            cropsLeftUIText[i].text = "Crops Left" + "\n <i>" + cropsLeft[i] + "</i>";
            //Crops Cut UI
            cropsCutUIText[i].text = "Crops Cut" + "\n <i>" + cropsCut[i] + "</i>";
            //Crops Cut UI
            cropsPlantedUIText[i].text = "Crops Planted" + "\n <i>" + cropsPlanted[i] + "</i>";
        }


    }

    List<int> sortedPositions = new List<int>();
    void SetPositionEndGameUI()
    {
        sortedPositions.Clear();
        for (int i = 0; i < cropsLeft.Count; i++)
        {
            sortedPositions.Add(cropsLeft[i]);
        }

        sortedPositions.Sort();
        sortedPositions.Reverse();

        CheckWinPosition();
        CheckIfBest();


    }

    //Really dumb way of finding first players should use dictionary instead tbh

    void CheckWinPosition()
    {
        for (int i = sortedPositions.Count - 1; i > -1; i--)
        {
            if (i == 0)
            {
                for (int x = 0; x < cropsLeft.Count; x++)
                {
                    if (cropsLeft[x] == sortedPositions[i])
                    {
                        positionUIText[x].text = "WINNER";
                        cropsLeftUIText[x].text = "<color=#FFF100><size=40>★</size></color> \n" + cropsLeftUIText[x].text;
                    }
                }
            }
            else if (i == 1)
            {
                for (int x = 0; x < cropsLeft.Count; x++)
                {
                    if (cropsLeft[x] == sortedPositions[i])
                    {
                        positionUIText[x].text = "2nd";
                    }
                }
            }
            else if (i == 2)
            {
                for (int x = 0; x < cropsLeft.Count; x++)
                {
                    if (cropsLeft[x] == sortedPositions[i])
                    {
                        positionUIText[x].text = "3rd";
                    }
                }
            }
            else if (i == 3)
            {
                for (int x = 0; x < cropsLeft.Count; x++)
                {
                    if (cropsLeft[x] == sortedPositions[i])
                    {
                        positionUIText[x].text = "4th";
                    }
                }
            }
        }

    }

    void CheckIfBest()
    {
        int highestCropsCut = Mathf.Max(cropsCut.ToArray());
        int highestCropsPlanted = Mathf.Max(cropsPlanted.ToArray());

        for (int i = 0; i < cropsCut.Count; i++)
        {
            if (cropsCut[i] == highestCropsCut)
            {
                cropsCutUIText[i].text = "<color=#FFF100><size=40>★</size></color> \n" + cropsCutUIText[i].text;
            }

            if (cropsPlanted[i] == highestCropsPlanted)
            {
                cropsPlantedUIText[i].text = "<color=#FFF100><size=40>★</size></color> \n" + cropsPlantedUIText[i].text;
            }

        }


    }


    void PopUpGameEndUI()
    {
        endGameUI.transform.localScale = Vector3.zero;
        endGameUI.SetActive(true);
        endGameUI.transform.DOScale(Vector3.one, 0.6f).SetEase(Ease.OutBack);
    }

    IEnumerator PopOutEndGameUI()
    {
        endGameUI.transform.DOScale(Vector3.zero, 0.6f).SetEase(Ease.OutBack);
        yield return new WaitForSeconds(1);

        endGameUI.SetActive(false);
    }


    List<int> cropsLeft = new List<int>();
    List<int> cropsCut = new List<int>();
    List<int> cropsPlanted = new List<int>();


    void GetAllPlayerInfo()
    {
        cropsLeft.Clear();
        cropsCut.Clear();
        cropsPlanted.Clear();

        for (int i = 0; i < playerCount; i++)
        {
            cropsLeft.Add(PoolManager.instance.pools["Player" + (i + 1)].CurrentAliveGrassNumber());
            cropsCut.Add(PlayerManager.instance.listOfPlayers[i].GetComponentInChildren<Scythe>(true).cutCount);
            cropsPlanted.Add(PlayerManager.instance.listOfPlayers[i].GetComponentInChildren<SpinningBag>(true).totalSeedPlanted);
        }



    }





}
