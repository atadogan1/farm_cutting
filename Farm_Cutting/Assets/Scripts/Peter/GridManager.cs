﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    public float rows;
    public float colums;

    public float gridSize;
    public float randomness;

    public int maxGrassNumOnSingleGrid;

    public Dictionary<string, Grid> grid = new Dictionary<string, Grid>();
    public Dictionary<Grid, int> currentGrass = new Dictionary<Grid, int>();

    public bool drawGizmo;

    public static GridManager instance;


    public void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        grid.Clear();

        for (int x = 0; x < rows; x++)
        {
            for (int y = 0; y < colums; y++)
            {
                Vector3 gridWorldPos = new Vector3(transform.position.x + (x * gridSize), transform.position.y, transform.position.z + (y * gridSize));

                Vector2 gridCoordinate = new Vector2(x, y);
                string gridCoordinateToSting = (int)gridCoordinate.x + "," + (int)gridCoordinate.y;

                Grid newGrid = new Grid(gridWorldPos, gridCoordinate);

                grid.Add(gridCoordinateToSting, newGrid);
                currentGrass.Add(newGrid, 0);
            }
        }
    }

    public Grid GridDetection(Vector3 position)
    {
        Vector2 playerPos = new Vector2(position.x, position.z);
        Vector2 playerCoordAfterRecalculateSize = new Vector2(Mathf.Round((playerPos.x - transform.position.x) / gridSize), Mathf.Round((playerPos.y - transform.position.z) / gridSize));

        string playerCoord = playerCoordAfterRecalculateSize.x + "," + playerCoordAfterRecalculateSize.y;

        Grid tempGrid;

        if (grid.TryGetValue(playerCoord, out tempGrid))
        {
            return tempGrid;
        }
        else
        {
            return grid["0,0"];
        }
    }


    private void OnDrawGizmos()
    {
        if (Application.isPlaying && drawGizmo)
        {
            foreach (Grid item in grid.Values)
            {
                Gizmos.DrawWireSphere(item.gridCenterWorldPos, gridSize / 5);
            }
        }
    }

}

public struct Grid
{
    public Vector3 gridCenterWorldPos;

    public Vector2 coordinate;

    public Grid(Vector3 worldPos, Vector2 coordinatePos)
    {
        gridCenterWorldPos = worldPos;
        coordinate = coordinatePos;
    }

}

