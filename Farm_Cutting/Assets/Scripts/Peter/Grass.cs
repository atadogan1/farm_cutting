﻿using UnityEngine;
using System.Collections;
using Sirenix.OdinInspector;

public class Grass : SerializedMonoBehaviour
{

    ConfigurableJoint configJoint;
    Rigidbody m_Rigidbody;
    Material m_Material;
    CapsuleCollider[] m_CapCollider;

    public int playerNumber;

    float originalSize;

    public bool alreadyCut;

    void Awake()
    {
        configJoint = GetComponent<ConfigurableJoint>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Material = GetComponentInChildren<Renderer>().material;
        m_CapCollider = GetComponentsInChildren<CapsuleCollider>();
        originalSize = transform.localScale.y;
    }

    void Start()
    {
        //GotCut(new Vector3(Random.Range(1f, 2f), Random.Range(1f, 2f), 0));
    }

    void OnEnable()
    {
        StartCoroutine(Grow(0.5f));
    }
    
    public void GotCut(Vector3 cutDirection, int playerNum)
    {
        if(playerNum != playerNumber)
        {
            alreadyCut = true;
            PoolManager.instance.pools["Player" + playerNumber].pendingCutGrass++;
            GridManager.instance.currentGrass[GridManager.instance.GridDetection(transform.position)]--;

            configJoint.angularXMotion = ConfigurableJointMotion.Free;
            configJoint.angularYMotion = ConfigurableJointMotion.Free;
            configJoint.angularZMotion = ConfigurableJointMotion.Free;
            configJoint.xMotion = ConfigurableJointMotion.Free;
            configJoint.yMotion = ConfigurableJointMotion.Free;
            configJoint.zMotion = ConfigurableJointMotion.Free;

            GetComponent<Rigidbody>().AddForceAtPosition(cutDirection, transform.position + transform.localScale.y * Vector3.up, ForceMode.Impulse);

            StartCoroutine(SinkDown(2));
        }
    }

    IEnumerator SinkDown(float wait)
    {
        yield return new WaitForSeconds(wait);
        
        DisableAll();
        
        float timer = 0;
        while (timer < 0.99f)
        {
            transform.position += Vector3.down/5 * Time.deltaTime;

            m_Material.SetFloat("_Transparency", timer);

            timer += Time.deltaTime/2;

            yield return null;
        }

        ResetAll();
    }

    void DisableAll()
    {
        m_Rigidbody.isKinematic = true;
        
        foreach (CapsuleCollider item in m_CapCollider)
        {
            item.enabled = false;
        }
    }

    void ResetAll()
    {

        transform.rotation = Quaternion.identity;

        configJoint.angularXMotion = ConfigurableJointMotion.Limited;
        configJoint.angularYMotion = ConfigurableJointMotion.Limited;
        configJoint.angularZMotion = ConfigurableJointMotion.Limited;
        configJoint.xMotion = ConfigurableJointMotion.Locked;
        configJoint.yMotion = ConfigurableJointMotion.Locked;
        configJoint.zMotion = ConfigurableJointMotion.Locked;

        foreach (CapsuleCollider item in m_CapCollider)
        {
            item.enabled = true;
        }

        m_Rigidbody.isKinematic = false;
        m_Material.SetFloat("_Transparency", 0);
        
        gameObject.SetActive(false);
        PoolManager.instance.pools["Player" + playerNumber].pendingCutGrass--;
        alreadyCut = false;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="growSpeed"> Grow speed per unit per second </param>
    IEnumerator Grow(float growSpeed)
    {
        //configJoint.angularXMotion = ConfigurableJointMotion.Locked;
        //configJoint.angularYMotion = ConfigurableJointMotion.Locked;
        //configJoint.angularZMotion = ConfigurableJointMotion.Locked;

        float timer = 0;

        transform.localScale = Vector3.zero;

        while(transform.localScale.y < originalSize)
        {
            transform.localScale += Vector3.one * growSpeed * Time.deltaTime;

            timer += Time.deltaTime;

            yield return null;
        }

        configJoint.angularXMotion = ConfigurableJointMotion.Limited;
        configJoint.angularYMotion = ConfigurableJointMotion.Limited;
        configJoint.angularZMotion = ConfigurableJointMotion.Limited;
    }
}
