﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantSeeds : MonoBehaviour
{
    //spublic string seedPoolKey;

    public static void PlantSeed(Grid grid, string seedPoolKey, int playerNumber)
    {
        GameObject obj = PoolManager.instance.pools[seedPoolKey].GetPooledObjects();
        obj.SetActive(true);
        obj.GetComponentInChildren<Grass>().playerNumber = playerNumber;

        Vector3 randomness = new Vector3(Random.insideUnitCircle.x * GridManager.instance.randomness, 0, Random.insideUnitCircle.y * GridManager.instance.randomness);

        obj.transform.position = grid.gridCenterWorldPos + randomness;
        obj.transform.eulerAngles = new Vector3(0, Random.Range(0f, 360f), 0);
        obj.GetComponent<ConfigurableJoint>().connectedAnchor = grid.gridCenterWorldPos + randomness;

        PlayerManager.instance.listOfPlayers[playerNumber - 1].GetComponentInChildren<SpinningBag>(true).totalSeedPlanted++;
    }

}
