﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{

    public static T instance;

    public virtual void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Duplicate subclass of type " + typeof(T) + "! eliminating " + name + " while preserving " + instance.name);
            Destroy(gameObject);
        }
        else
        {
            instance = this as T;
            DontDestroyOnLoad(instance);
        }
    }

    public virtual void OnDestroy()
    {
        if (instance == this)
            instance = null;
    }

}
