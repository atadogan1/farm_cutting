﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    public float speed;
    public float rotationSpeed;
    public float deceleration;

    private float acceleration;

    public void Move(Vector3 direction, Transform self, Rigidbody rb)
    {
        if (direction != Vector3.zero)
        {
            self.eulerAngles = new Vector3(
                Mathf.LerpAngle(self.eulerAngles.x, 0, rotationSpeed),
                Mathf.LerpAngle(self.eulerAngles.y, Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg, rotationSpeed),
                Mathf.LerpAngle(self.eulerAngles.z, 0, rotationSpeed));

            acceleration += speed * Time.deltaTime;
        }
        else
        {
            acceleration -= deceleration * Time.deltaTime;
        }

        acceleration = Mathf.Clamp01(acceleration);

        rb.MovePosition(transform.position + self.transform.forward * speed * acceleration * Time.deltaTime);
    }

}
