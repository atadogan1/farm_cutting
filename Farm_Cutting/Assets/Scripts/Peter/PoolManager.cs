﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class PoolManager : SerializedMonoBehaviour
{
    public static PoolManager instance;

    public Dictionary<string, Pool> pools;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        foreach (string key in pools.Keys)
        {
            pools[key].InitializeObjectPool();
        }
    }

    [Button]
    void ManualInitializePools()
    {
        foreach (string key in pools.Keys)
        {
            pools[key].InitializeObjectPool();
        }
    }

    [Button]
    void ClearPooledGameObjects(string poolKey = "")
    {
        if (poolKey == "")
        {
            foreach (string key in pools.Keys)
            {
                pools[key].pooledGameObjects.Clear();
            }
        }
        else
        {
            pools[poolKey].pooledGameObjects.Clear();
        }
    }

    public class Pool
    {
        public string name;

        public int initAmount;

        public Transform parent;
        public GameObject gameObjectPrefab;
        public List<GameObject> pooledGameObjects = new List<GameObject>();
        public bool canGrow;

        public int pendingCutGrass;

        public void InitializeObjectPool()
        {
            for (int i = 0; i < initAmount; i++)
            {
                GameObject obj = Instantiate(gameObjectPrefab) as GameObject;
                obj.transform.parent = parent;
                obj.SetActive(false);
                pooledGameObjects.Add(obj);
            }
        }

        public GameObject GetPooledObjects()
        {
            foreach (GameObject item in pooledGameObjects)
            {
                if (!item.activeInHierarchy)
                {
                    return item;
                }
            }

            if (canGrow)
            {
                GameObject obj = Instantiate(gameObjectPrefab) as GameObject;
                obj.transform.parent = parent;
                obj.SetActive(false);
                pooledGameObjects.Add(obj);
                return obj;
            }

            return null;
        }

        public int CurrentAliveGrassNumber()
        {
            int num = 0;
            foreach (GameObject item in pooledGameObjects)
            {
                if(item.activeInHierarchy)
                {
                    num++;
                }
            }

            //print("Current Alive Grass Number; " + num);
            return num - pendingCutGrass;
        }
    }


}

