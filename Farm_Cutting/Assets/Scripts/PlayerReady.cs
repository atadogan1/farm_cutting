﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;
using DG.Tweening;
using Sirenix.OdinInspector;


public class PlayerReady : MonoBehaviour {

    [SerializeField]
    GameObject[] playerModels;
    [SerializeField]
    GameObject[] playerText;

    

    // Use this for initialization
    void Start () 
	{
		StartCoroutine(CheckIfAllControllersConnected());

        for ( int i =0; i< playerModels.Length; i++)
        {
            playerModels[i].SetActive(false);
           
        }
        foreach (GameObject playertext in playerText)
        {
            playertext.SetActive(false);
        }

        DOTween.defaultEaseOvershootOrAmplitude = 1.2f;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    [Button("Ready Up")]
    void TestReadyUp()
    {
        for (int i = 0; i < playerNeededCount; i++)
        {
            playerReady[i] = true;
            ReadyUpPlayer(i);
        }
    }

	[SerializeField]
	int playerNeededCount = 4;
	IEnumerator CheckIfAllControllersConnected()
	{

		while(true)
		{
			for (int i = 0; i < 4; i++)
			{
				CheckIfPlayerActive(i+1);
			}

			if(activePlayers >= playerNeededCount)
			{
				break;
			}

			yield return 0;
		}

		//call start game function here
		GameManager.instance.StartGame();
		yield return null;

	}

	[SerializeField]

    public List<GameObject> playerReadyUI = new List<GameObject>();

	[SerializeField]
    bool[] playerReady = new bool[4];
	[SerializeField]
    int activePlayers;
    void CheckIfPlayerActive(int playerNum)
    {
        if (XCI.GetButtonDown(XboxButton.A, (XboxController)playerNum) || Input.GetButtonDown("Jump"))
        {
			if(!playerReady[playerNum-1])
			{
              //  Debug.Log(playerNum);
				playerReady[playerNum - 1] = true;
				ReadyUpPlayer(playerNum - 1);
            	activePlayers++;
			}
        }
    }



	//Here is where we get if they readied up
	void ReadyUpPlayer(int playerNum)
	{
        //Bring Up Player UI
        playerModels[playerNum].SetActive(true);
        playerText[playerNum].SetActive(true);

        playerModels[playerNum].transform.localScale = Vector3.zero;
        playerModels[playerNum].transform.DOScale(Vector3.one * 0.4f, 1.2f)
            .SetEase(Ease.OutBounce);

    }
}
