﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(PlayerController))]
public class PlayerMovement : MonoBehaviour
{
    private Rigidbody rbPlayer;

    private Vector3 movementInput = Vector3.zero;
    private Vector3 rotationInput =  Vector3.zero;
    private float angleInput = 0;

    public float minCutSpeedNeeded = 10;
    public float maxCutSpeedNeeded = 90;
    public TrailRenderer trail;

    private float lastAngleInput = 0;
    public float lastAngleDiff = 0;

    private int angleAvgSampleCount = 5;
    private List<float> angleInputs = new List<float>();
    private float angleInputAvg;
    private float[] angleDiffs;
    public float angleDiffAvg;


    private Vector3 currentVelocity;
    private Vector3 velocityDir = Vector3.zero;
    private float currentRotation;




    public bool spin;


    [SerializeField]
    private float maxSpeed = 1;
    //private float maxSpeedSquared;
    [SerializeField]
    private float timeToMaxSpeed = 1;
    private float accelerationRate;
    [SerializeField]
    private float timeToZeroSpeed = 1;
    private float decelerationRate;
   // [SerializeField]
    //private float timeToTurn = 1 ;
    [SerializeField]
    private float rotationRate; //radians
    [SerializeField]
    private float rotationDecayRate;

    void Awake()
    {
       
        rbPlayer = GetComponent<Rigidbody>();
        // maxSpeedSquared = maxSpeed * maxSpeed;
        accelerationRate = maxSpeed / timeToMaxSpeed;
        decelerationRate = maxSpeed / timeToZeroSpeed;
        // rotationRate = 2 * Mathf.PI * Mathf.Rad2Deg / timeToTurn ; //radians
        rbPlayer.maxAngularVelocity = 200;
        rbPlayer.centerOfMass = Vector3.zero;
        angleDiffs = new float[angleAvgSampleCount-1];
    }

    void FixedUpdate()
    {

        DoMovement();
        DoRotation();
        

    }

    void DoMovement()
    {
        
        currentVelocity = new Vector3 (rbPlayer.velocity.x, 0, rbPlayer.velocity.z);

        float velocityMag = currentVelocity.magnitude;

        if(velocityMag!=0) velocityDir = currentVelocity / velocityMag;

        //Debug.Log("Input: " + movementInput);

        if (movementInput != Vector3.zero)//check if there is input
        {
            if (velocityMag < maxSpeed)
            {//if not max speed
                velocityMag += accelerationRate * Time.fixedDeltaTime;
            }


            velocityDir = movementInput.normalized;
            //velocityDir = Vector3.ClampMagnitude(movementInput, 1);


        }
        else //no input
        {
            if (velocityMag != 0) //if not zero speed
            {
                if (Mathf.Abs(velocityMag) < decelerationRate * Time.fixedDeltaTime)
                {
                    velocityMag = 0;
                }
                else
                {
                    velocityMag -= decelerationRate * Time.fixedDeltaTime;
                }
            }

        }

        velocityMag = Mathf.Clamp(velocityMag,0,  maxSpeed);

        currentVelocity = velocityMag * velocityDir;

        if (rbPlayer.velocity.y < 0)
        {
            currentVelocity.y = rbPlayer.velocity.y + Physics.gravity.y * Time.fixedDeltaTime ;
        }

        //  rbPlayer.velocity = currentVelocity * (  Mathf.Clamp01( movementInput.magnitude) );
        rbPlayer.velocity = currentVelocity;

    }



    //works without force
    
    void DoRotation()
    {

        currentRotation = rbPlayer.rotation.eulerAngles.y;


        angleInput = Mathf.Atan2(rotationInput.x, rotationInput.z) * Mathf.Rad2Deg;

        if (angleInput != currentRotation && rotationInput!=Vector3.zero)//different rotation angly and input isnt zero
        {

            currentRotation = Mathf.LerpAngle(currentRotation, angleInput, rotationRate * Mathf.Rad2Deg * Time.fixedDeltaTime);
            rbPlayer.MoveRotation (Quaternion.Euler(0,currentRotation, 0));
            lastAngleDiff =  Mathf.Clamp(angleInput -lastAngleInput, -5,5);

        }
        else if (rotationInput == Vector3.zero )
        {

            if (movementInput!=Vector3.zero)
            {
                float facingAngle = Mathf.Atan2(movementInput.x, movementInput.z) * Mathf.Rad2Deg;
               

                currentRotation = Mathf.LerpAngle(currentRotation, facingAngle, rotationRate * Mathf.Rad2Deg * Time.fixedDeltaTime);
            }
            if(lastAngleDiff!=0 )
            {
                currentRotation += lastAngleDiff * Time.fixedDeltaTime * 100;
                
            }
            rbPlayer.MoveRotation(Quaternion.Euler(0, currentRotation, 0));


            if (Mathf.Abs(lastAngleDiff) < rotationDecayRate * Time.fixedDeltaTime) lastAngleDiff = 0;

            else lastAngleDiff = (Mathf.Abs(lastAngleDiff) - rotationDecayRate * Time.fixedDeltaTime) * Mathf.Sign(lastAngleDiff);
            

        }


        lastAngleInput = rbPlayer.rotation.eulerAngles.y;

    }




    //=========kinda works, changing it to rigid.torquevelocity
    /*
        void DoRotation()
        {
            

            currentRotation = rbPlayer.rotation.eulerAngles.y;


            if(rotationInput == Vector3.zero)
            {
                angleInput = 0;
            }
            else
            {
                angleInput = Mathf.Atan2(rotationInput.x, rotationInput.z) * Mathf.Rad2Deg;
            }


            while (angleInput < 0)
            {
                angleInput += 360;
            }


            Debug.Log(rbPlayer.angularVelocity);



            if (lastAngleInput!= angleInput && rotationInput!=Vector3.zero)
            {
                float angleDiff = Mathf.DeltaAngle( angleInput, lastAngleInput);

                bool spin = false;

                if (Mathf.Abs(angleDiff) < 10) spin = false;
                if (Mathf.Abs(angleDiff) > 70) spin = false;
                if (Mathf.Abs(angleDiff) > 30) spin = true;


                if(spin)  rbPlayer.AddTorque(new Vector3(0, 600, 0)  * Mathf.Sign(-angleDiff) , ForceMode.Acceleration);
                lastAngleInput = angleInput;
            }


        }
        */



    //works with rotation
    /* rotationrate = 7
     * rigidbody spin is enabled on y


void DoRotation()
{

    float maxAngVelocity = 200;




    if (lastAngleInput != angleInputAvg && rotationInput != Vector3.zero)
    {

        angleDiffAvg = 0;
        for (int i = 0; i < angleInputs.Count-1; i++)
        {
            angleDiffs[i] = Mathf.DeltaAngle(angleInputs[i], angleInputs[i+1]);

        }

        for (int i = 0; i < angleInputs.Count-1; i++)
        {
            angleDiffAvg += angleDiffs[i];
        }




        angleDiffAvg /= angleInputs.Count;



        bool spin = true;

       // if (Mathf.Abs(angleDiff) < 5) spin = true;
       // if (Mathf.Abs(angleDiff) >= 5) spin = true;
        if (Mathf.Abs(angleDiffAvg) >= 130) spin = false;



        //if (spin) rbPlayer.AddTorque(new Vector3(0, 600, 0) * Mathf.Sign(-angleDiff), ForceMode.Acceleration);
        if (spin)
        {


            float angVelocity = rbPlayer.angularVelocity.y;
            //Debug.Log(angVelocity);

            if (Mathf.Sign(angVelocity)== Mathf.Sign(-angleDiffAvg) || angVelocity ==0)
            {
                if (angVelocity != maxAngVelocity)
                {
                    angVelocity += rotationRate * Mathf.Sign(-angVelocity) * Time.fixedDeltaTime  * (Mathf.Abs(angleDiffAvg));
                   // Debug.Log(rbPlayer.angularVelocity+" || "+ rotationRate * Mathf.Sign(angVelocity) * Time.fixedDeltaTime * (Mathf.Abs(angleDiffAvg)));
                    Debug.Log(rbPlayer.angularVelocity + " || " +  (Mathf.Abs(angleDiffAvg)));
                }
            }
            else
            {
                angVelocity -= rotationRate * Mathf.Sign(-angVelocity) * Time.fixedDeltaTime * (Mathf.Abs(angleDiffAvg));
               // Debug.Log("nope");
            }

            //angVelocity = Mathf.Clamp(angVelocity, -maxAngVelocity, maxAngVelocity);


            rbPlayer.angularVelocity = new Vector3(0, angVelocity, 0);


        }

        lastAngleInput = angleInputAvg;
    }




}*/

    public void SetMovementInput(Vector3 _movementInput)
    {
        movementInput = _movementInput;
    }

    public void SetRotationInput(Vector3 _rotationInput)
    {
        rotationInput = _rotationInput;
        UpdateRotationAvg();

    }



    public void UpdateRotationAvg()
    {
        angleInputs.Add(angleInput);



        if (angleInputs.Count == angleAvgSampleCount)
        {

            angleInputs.RemoveAt(0);

        }



        float angleInputAvgHolden = 0; //this is so you dont use the avg while its being calculated

        for (int i = 0; i < angleInputs.Count; i++)
        {


            angleInputAvgHolden += angleInputs[i];
        }

        angleInputAvg = angleInputAvgHolden / angleInputs.Count;

        UpdateRotationDiffAvg();
    }


    public void UpdateRotationDiffAvg()
    {

        angleDiffAvg = 0;

        for (int i = 0; i < angleInputs.Count - 1; i++)
        {
            angleDiffs[i] = Mathf.DeltaAngle(angleInputs[i], angleInputs[i + 1]);

        }

        for (int i = 0; i < angleInputs.Count - 1; i++)
        {
            angleDiffAvg += angleDiffs[i];
        }




        angleDiffAvg /= angleInputs.Count;



        spin = false;
        
        // if (Mathf.Abs(angleDiffAvg) < 5) spin = true;
        if (Mathf.Abs(angleDiffAvg) >= minCutSpeedNeeded) spin = true;
        if (Mathf.Abs(angleDiffAvg) >= maxCutSpeedNeeded) spin = false;
        //Debug.Log(Mathf.Abs(angleDiffAvg));

        DoTrail();

        //if (spin) rbPlayer.AddTorque(new Vector3(0, 600, 0) * Mathf.Sign(-angleDiff), ForceMode.Acceleration);

    }

    void DoTrail()
    {
        if (spin && !trail.emitting )
        {
            trail.emitting = true;
        }
        if(!spin && trail.emitting)
        {
            trail.emitting = false;

        }
    }
        //this is for final spin mode
        /*
        if (rotationInput == Vector3.zero)
        {
            angleInput = 0;
        }
        else
        {
            angleInput = Mathf.Atan2(rotationInput.x, rotationInput.z) * Mathf.Rad2Deg;
        }


        while (angleInput < 0)
        {
            angleInput += 360;
        }

       

        angleInputs.Add(angleInput);
       


        if (angleInputs.Count == angleAvgSampleCount)
        {
           
            angleInputs.RemoveAt(0);
        
        }
       


        float angleInputAvgHolden = 0; //this is so you dont use the avg while its being calculated
       
        for (int i = 0; i < angleInputs.Count; i++)
        {
            

            angleInputAvgHolden += angleInputs[i];
        }
        angleInputAvg = angleInputAvgHolden / angleInputs.Count;
        */
        

    


    /*
    [SerializeField]
    private float impactVelocityRate = 1;
    [SerializeField]
    private float impactAngularRate = 1;

    //this is when it hits another scythe
    public void RepelScythe(Rigidbody _rbScytheHit)
    {
        Debug.Log("bouncing scythes " + _rbScytheHit.transform.GetComponent<PlayerMovement>().lastAngleDiff);
        //if (_rbScytheHit.angularVelocity != Vector3.zero)
        //{
        //    rbPlayer.rotation = Quaternion.Euler(0, Mathf.Clamp(_rbScytheHit.transform.GetComponent<PlayerMovement>().lastAngleDiff,-0.5f, 0.5f ) * impactAngularRate * Mathf.Deg2Rad + rbPlayer.rotation.y,0);
        // }
        rbPlayer.AddForce( _rbScytheHit.position - rbPlayer.position * impactVelocityRate, ForceMode.Impulse);



    }
    */

    //use this when you hit walls
   /* public void RepelScythe()
    {
        Debug.Log("repel scythe like a wall " + -rbPlayer.angularVelocity * impactAngularRate);
        rbPlayer.AddTorque(-rbPlayer.angularVelocity * impactAngularRate, ForceMode.Impulse);

       
    }*/


        /*
    //got hit by a mans with body
    public void GotHitByAMans(Rigidbody _rbPlayerHit)
    {
        Debug.Log("git bumped by a mans body " + _rbPlayerHit.velocity * impactVelocityRate);
        if (_rbPlayerHit.velocity != Vector3.zero)
        {
            rbPlayer.AddForce( _rbPlayerHit.velocity * impactVelocityRate, ForceMode.Impulse);
            
        }
    }

    public void GotHitByAMansScythe(Rigidbody _rbScytheHit)
    {
        
        Debug.Log("ouch " + _rbScytheHit);
        if (_rbScytheHit.angularVelocity != Vector3.zero)
        {
            
            rbPlayer.AddForce(- (rbPlayer.position - _rbScytheHit.position).normalized  * impactVelocityRate, ForceMode.Impulse);

            
        }
    }*/

   /* void OnCollisionEnter(Collision collision)
    {




        if (this.gameObject.CompareTag("Player"))
        {

            //if the thing we hit is a player with our body
            if (collision.collider.gameObject.CompareTag("Player"))

            {
                Debug.Log(collision.contacts)
                // Debug.Log("player " + collision.gameObject.GetComponentInParent<PlayerController>().playerNumber + "  is freaking struggling man");

                //Debug.Log("this is a body of  " + collision.gameObject.GetComponentInParent<PlayerController>().playerNumber + " hitting " + this.gameObject.tag);

                collision.gameObject.GetComponentInParent<PlayerMovement>().GotHitByAMans(GetComponent<Rigidbody>());
            }


            //if the thing we hit is a scythe
            if (collision.collider.gameObject.CompareTag("Scythe"))

            {

                //  do nothing when we hit their sctyhe, we already get bumped

                //Debug.Log("this is a body of  " + collision.gameObject.GetComponentInParent<PlayerController>().playerNumber + " hitting  scythe of " + this.gameObject.tag);

                //collision.gameObject.GetComponentInParent<PlayerMovement>().GotCutByScythe(GetComponent<Rigidbody>());


            }
        }
    }*/

}

